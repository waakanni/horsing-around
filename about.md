---
title: About
layout: page
permalink: /about/
---

This site was generated with GitLab Pages on [git.embl.de][embl-gitlab], the GitLab system hosted by
the EMBL [Bio-IT Project][bio-it]. The associated project repository is [here][horsing-around] and this
site is built from the *gitlabpages* branch. The repository uses [Jekyll][jekyll] to build HTML from
[Markdown][markdown] files.

You can use GitLab Pages to build and host static webpages from [git.embl.de][embl-gitlab]. Pages will
be hosted from `https://<username>.embl-community.io/<projectname>`, where `<username>` and `<projectname>`
are your EMBL username and the GitLab project namespace respectively. You don't have to use Jekyll to
build your HTML pages: you can include raw HTML files in the project repository or choose one of the
[other engines][build-options] available to generate content from text files.

This site and the associated repository are used for training sessions in intermediate Git usage and
Pages/continuous integration with GitHub and/or GitLab. Check out the [Bio-IT homepage]i[bio-it] for a
listing of all our upcoming courses and events.

[bio-it]: https://bio-it.embl.de/
[build-options]: https://gitlab.com/pages
[embl-gitlab]: https://git.embl.de/
[horsing-around]: https://git.embl.de/stamper/horsing-around/
[jekyll]: https://jekyllrb.com/
[markdown]:https://www.markdownguide.org/
